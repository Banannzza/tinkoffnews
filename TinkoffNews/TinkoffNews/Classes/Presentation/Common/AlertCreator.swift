//
//  AlertCreator.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 21/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

struct AlertCreator {
    
    private init() {}
    
    static func downloadAlert(title: String?, message: String?) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        return alert
    }
    
    static func errorAlert(title: String?, message: String?) -> UIAlertController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ок", style: .cancel) { (_) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(okAction)
        return alert
    }
    
}
