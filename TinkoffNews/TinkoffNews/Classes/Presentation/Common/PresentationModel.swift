//
//  PresentationModel.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 19/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

class PresentationModel {
    
    enum State {
        case empty
        case rich
        case loading
        case error(message: String)
        
        func isEqual(state: State) -> Bool {
            switch (self, state) {
                case (.empty, .empty),
                     (.rich, .rich),
                     (.loading, .loading):
                return true
            case (.error(let lhsMsg), .error(let rhsMsg)):
                return lhsMsg == rhsMsg
            default:
                return false
            } 
        }
    }
    
    weak var stateChangeHandler: PresentationModelDelegate?
    
    var state: State = .empty {
        didSet { self.stateChangeHandler?.presentationModelStateChanged(on: state) }
    }
    
}

protocol PresentationModelDelegate: class {
    func presentationModelStateChanged(on state: PresentationModel.State)
}

