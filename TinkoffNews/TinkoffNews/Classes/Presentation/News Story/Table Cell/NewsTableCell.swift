//
//  NewsTableCell.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 20/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class NewsTableCell: UITableViewCell {
    
    //MARK: - IBOutlet
    
    @IBOutlet private weak var countLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!
    @IBOutlet private weak var contentLabel: UILabel!
    
    //MARK: - Lifecycle
    
    override func prepareForReuse() {
        super.prepareForReuse()
        contentLabel.text = ""
        dateLabel.text = ""
        countLabel.text = ""
    }
    
    //MARK: - Configure
    
    func configure(viewModel: NewsViewModel) {
        contentLabel.text = viewModel.text
        dateLabel.text = viewModel.date
        countLabel.text = viewModel.tapCount
    }
    
    
}
