//
//  NewsTablePresentationModel.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 20/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

class NewsTablePresentationModel: PresentationModel {
    
    //MARK: - Property
    
    private var sectionIndex: Int
    private let section = 20
    
    var viewModel: [NewsViewModel]
    
    override init() {
        sectionIndex = 1
        viewModel = []
    }
    
    //MARK: - Methods
    
    func nextNews() {
        guard !self.state.isEqual(state: .loading) else { return }
        self.state = .loading
        let first = max(0, sectionIndex - 1) * section
        let last = sectionIndex * section
        ServiceLayer.shared.newsService.newsList(first: first, last: last) { (response) in
            switch response {
            case .fail(let error):
                self.state = .error(message: error?.localizedDescription ?? "Что-то пошло не так")
            case .success(let news):
                self.sectionIndex += 1
                let viewModels = news.map({ NewsViewModel(news: $0) })
                self.appendNewViewModels(viewModels)
                self.state = .rich
            }
        }
    }
    
    func savedNews() -> [NewsViewModel] {
        self.viewModel = ServiceLayer.shared.newsService.savedNewsList().map({ NewsViewModel(news: $0) })
        return self.viewModel
    }
    
    func updateNewsTapCount(news: NewsViewModel) {
        if let index = viewModel.index(of: news) {
            viewModel[index] = NewsViewModel(news: ServiceLayer.shared.newsService.updateNewsTapCounter(newsId: news.id))
        }
    }
    
    private func appendNewViewModels(_ newsList: [NewsViewModel]) {
        let localNews = Set<NewsViewModel>(viewModel.map({ $0 }))
        let newNews = Set<NewsViewModel>(newsList.map({ $0 }))
        let newsForAppend = newNews.subtracting(localNews)
        self.viewModel.insert(contentsOf: newsForAppend, at: 0)
        self.viewModel = self.viewModel.sorted(by: { (firstNews, secondNews) -> Bool in
            return firstNews.dateTimestamp > secondNews.dateTimestamp
        })
    }
    
}
