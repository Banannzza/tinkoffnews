//
//  NewsTableViewController.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 19/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class NewsTableViewController: UITableViewController {
    
    //MARK: - Property
    
    private let presentationModel = NewsTablePresentationModel()
    private var alertView: UIAlertController?
    private lazy var router = NewsTableRouter(root: self)
    
    //MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        configurePresentationModel()
        configureUI()
        presentationModel.nextNews()
    }
    
    //MARK: - Configure
    
    private func configurePresentationModel() {
        presentationModel.stateChangeHandler = self
        _ = presentationModel.savedNews()
    }
    
    private func configureUI() {
        self.title = "Новости"
        tableView.estimatedRowHeight = 40
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(refreshData), for: UIControlEvents.valueChanged)
    }
    
    //MARK: - Common
    
    private func reloadTableView() {
        DispatchQueue.main.async {
            self.refreshControl?.endRefreshing()
            self.tableView.reloadData()
        }
    }
    
    private func showError(withMessage message: String?) {
        DispatchQueue.main.async {
            self.refreshControl?.endRefreshing()
            self.alertView?.dismiss(animated: true, completion: nil)
            self.alertView = AlertCreator.errorAlert(title: "Ошибка", message: message)
            self.present(self.alertView!, animated: true, completion: nil)
        }
    }
    
    @objc private func refreshData() {
        presentationModel.nextNews()
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presentationModel.viewModel.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let identifier = String(describing: NewsTableCell.self).components(separatedBy: ".").last!
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! NewsTableCell
        cell.configure(viewModel: presentationModel.viewModel[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let newsId = presentationModel.viewModel[indexPath.row].id
        presentationModel.updateNewsTapCount(news: presentationModel.viewModel[indexPath.row])
        tableView.reloadRows(at: [indexPath], with: .none)
        router.showNewsContent(newsId: newsId)
    }
}


//MARK: - PresentationModelDelegate

extension NewsTableViewController: PresentationModelDelegate {
    
    func presentationModelStateChanged(on state: PresentationModel.State) {
        switch state {
        case .loading:
            break
        case .error(let message):
            self.showError(withMessage: message)
        case .rich:
            self.alertView?.dismiss(animated: true, completion: nil)
            self.reloadTableView()
        default:
            break
        }
    }
    
}
