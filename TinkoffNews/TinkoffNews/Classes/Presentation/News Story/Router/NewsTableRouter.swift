//
//  NewsTableRouter.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 21/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class NewsTableRouter: Router {
    
    func showNewsContent(newsId: String) {
        let identifier = String(describing: NewsContentViewController.self).components(separatedBy: ".").last!
        self.show(storyboard: identifier, identifier: identifier) { (controller: NewsContentViewController) in
            controller.presentationModel = NewsContentPresentationModel(newsId: newsId)
        }
    }
    
}
