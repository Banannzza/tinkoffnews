//
//  NewsViewModel.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 20/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

struct NewsViewModel {
    
    let id: String
    let text: String
    let date: String
    let dateTimestamp: Double
    let tapCount: String
    
    init(news: NewsModel) {
        self.id = news.entityId
        self.text = news.text
        self.date = news.publicationDateString
        self.dateTimestamp = news.publicationDate["milliseconds"]!
        self.tapCount = "\(news.tapCount)"
    }
}

extension NewsViewModel: Hashable {
    
    var hashValue: Int {
        return text.hashValue
    }
    
    static func ==(lhs: NewsViewModel, rhs: NewsViewModel) -> Bool {
        return lhs.text == rhs.text && lhs.date == rhs.date
    }
    
}
