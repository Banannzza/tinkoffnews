//
//  NewsContentViewModel.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 21/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

struct NewsContentViewModel {
    
    //MARK: - Property
    
    var atrText: NSAttributedString?
    var text: String = ""
    
    //MARK: - Init
    
    init(htmlText: String) {
        do {
            let data = htmlText.data(using: .unicode)
            if let d = data {
                let str = try NSAttributedString(data: d,
                                                 options: [NSAttributedString.DocumentReadingOptionKey.documentType:  NSAttributedString.DocumentType.html],
                                                 documentAttributes: nil)
                atrText = str
            }
        } catch {
            text = htmlText
        }
    }
}
