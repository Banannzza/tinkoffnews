//
//  NewsContentPresentationModel.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 21/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

class NewsContentPresentationModel: PresentationModel {
    
    //MARK: - Property
    
    private let newsId: String

    var viewModel: NewsContentViewModel?
    
    init(newsId: String) {
        self.newsId = newsId
    }
    
    //MARK: - Common
    
    func obtainNewsContent() {
        self.state = .loading
        ServiceLayer.shared.newsService.newsContent(newsId: newsId) { (response) in
            switch response {
            case .fail(let error):
                self.state = .error(message: error?.localizedDescription ?? "Что-то пошло не так")
            case .success(let content):
                self.viewModel = NewsContentViewModel(htmlText: content)
                self.state = .rich
            }
        }
    }
    
}
