//
//  NewsContentViewController.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 21/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

class NewsContentViewController: UIViewController {
    
    //MARK: - IBOutlet
    
    @IBOutlet private weak var contentTextView: UITextView!
    
    //MARK: - Property
    
    private var alertView: UIAlertController?
    var presentationModel: NewsContentPresentationModel!
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presentationModel.stateChangeHandler = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presentationModel.obtainNewsContent()
    }
    
    //MARK: - Configure
    
    private func configureUI() {
//        self.navigationController?
    }
    
    //MARK: - Common
    
    private func showContent() {
        DispatchQueue.main.async { [weak self] in
            guard let `self` = self else { return }
            if let viewModel = self.presentationModel.viewModel {
                if let atrText = viewModel.atrText {
                    self.contentTextView.attributedText = atrText
                } else {
                    self.contentTextView.text = viewModel.text
                }
            }
        }
    }
    
    private func showError(withMessage message: String?) {
        DispatchQueue.main.async {
            self.alertView?.dismiss(animated: true, completion: nil)
            self.alertView = AlertCreator.errorAlert(title: "Ошибка", message: message)
            self.present(self.alertView!, animated: true, completion: nil)
        }
    }
}


//MARK: - PresentationModelDelegate

extension NewsContentViewController: PresentationModelDelegate {
    
    func presentationModelStateChanged(on state: PresentationModel.State) {
        switch state {
        case .loading:
            break
        case .error(let message):
            self.showError(withMessage: message)
        case .rich:
            self.alertView?.dismiss(animated: true, completion: nil)
            self.showContent()
        default:
            break
        }
    }
    
}
