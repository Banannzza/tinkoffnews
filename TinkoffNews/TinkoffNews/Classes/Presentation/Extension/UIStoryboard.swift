//
//  UIStoryboard.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 20/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import UIKit

public extension UIStoryboard {
    
    convenience init(name: String) {
        self.init(name: name, bundle: nil)
    }
    
}
