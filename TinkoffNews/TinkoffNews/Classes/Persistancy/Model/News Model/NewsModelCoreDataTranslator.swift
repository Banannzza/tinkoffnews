//
//  NewsModelCoreDataTranslator.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 19/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation
import CoreData

class NewsModelCoreDataTranslator<CDEntry: NSManagedObject>: CoreDataTranslator<CDEntry, NewsModel> {
    
    override func fill(_ entity: NewsModel, fromEntry: CDEntry) {
        entity.entityId = fromEntry.value(forKey: "entryId") as? String ?? ""
        entity.text = fromEntry.value(forKey: "text") as? String ?? ""
        entity.name = fromEntry.value(forKey: "name") as? String ?? ""
        entity.publicationDate = fromEntry.value(forKey: "publicationDate") as? [String: Double] ?? [:]
        entity.type = fromEntry.value(forKey: "type") as? Int ?? 0
        entity.tapCount = fromEntry.value(forKey: "tapCount") as? Int ?? 0
    }
    
    override func fill(_ entry: CDEntry, fromEntity: NewsModel, in context: NSManagedObjectContext) {
        entry.setValue(fromEntity.entityId, forKey: "entryId")
        entry.setValue(fromEntity.name, forKey: "name")
        entry.setValue(fromEntity.text, forKey: "text")
        entry.setValue(fromEntity.publicationDate, forKey: "publicationDate")
        entry.setValue(fromEntity.type, forKey: "type")
        entry.setValue(fromEntity.tapCount, forKey: "tapCount")
    }
    
}
