//
//  NewsModel.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 19/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation
import CoreData

final class NewsModel: Entity, Decodable {
    
    var name: String
    var text: String
    var publicationDate: [String: Double]
    var type: Int
    var tapCount: Int = 0
    
    var publicationDateString: String {
        if let milliseconds = publicationDate["milliseconds"] {
            let date = Date(timeIntervalSince1970: milliseconds / 1000.0)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            return dateFormatter.string(from:date as Date)
        } else {
            return ""
        }
    }
    
    enum CodingKeys: String, CodingKey
    {
        case entityId = "id"
        case name
        case text
        case publicationDate
        case type = "bankInfoTypeId"
    }
    
    required init() {
        self.name = ""
        self.text = ""
        self.publicationDate = [:]
        self.type = 0
        super.init()
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        do {
            let entityId = try values.decode(String.self, forKey: .entityId)
            self.name = try values.decode(String.self, forKey: .name)
            self.text = try values.decode(String.self, forKey: .text)
            self.publicationDate = try values.decode([String: Double].self, forKey: .publicationDate)
            self.type = try values.decode(Int.self, forKey: .type)
            super.init(entityId: entityId)
        }
        catch {
            throw error
        }
    }
    
    override func equals<T>(_ other: T) -> Bool where T: NewsModel {
        return self.text == other.text
    }
    
    
}

