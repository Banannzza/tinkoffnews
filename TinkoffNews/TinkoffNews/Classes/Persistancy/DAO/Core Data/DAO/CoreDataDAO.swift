//
//  CoreDataDAO.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 19/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation
import CoreData

class CoreDataDAO<CDModel: NSManagedObject, Model: Entity>: DAO<Model> {
    
    //MARK: - Property
    
    private let translator: CoreDataTranslator<CDModel, Model>
    private let persistentStoreCoordinator: NSPersistentStoreCoordinator
    
    
    private var context: NSManagedObjectContext {
        let context = Thread.isMainThread ?
            NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType) :
            NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        
        context.persistentStoreCoordinator = persistentStoreCoordinator
        context.shouldDeleteInaccessibleFaults = true
        if #available(iOS 10.0, *) {
            context.automaticallyMergesChangesFromParent = true
        }
        return context
    }
    
    init(containerName: String, translator: CoreDataTranslator<CDModel, Model>) throws {
        self.translator = translator
        
        let persistentContainer = NSPersistentContainer(name: containerName)
        let options = [NSMigratePersistentStoresAutomaticallyOption: true as NSObject,
                       NSInferMappingModelAutomaticallyOption: true as NSObject]
        
        persistentContainer.persistentStoreDescriptions
            .forEach { description in
                options
                    .forEach {
                        description.setOption($0.value, forKey: $0.key)
                }
                description.type = NSSQLiteStoreType
        }
        
        var error: Error?
        
        persistentContainer.loadPersistentStores { _, e in
            error = e
        }
        
        if let error = error { throw error }
        
        persistentStoreCoordinator = persistentContainer.persistentStoreCoordinator
        
        super.init()
    }
    
    //MARK: - DAO
    
    override open func persist(_ entity: Model) throws {
        var error: Error?
        
        let context = self.context
        
        context.performAndWait { [weak self] in
            guard let `self` = self else { return }
            
            do {
                if self.isEntryExist(entity.entityId, inContext: context) {
                    try self.update(entity, inContext: context)
                } else {
                    try self.create(entity, inContext: context)
                }
            } catch let e {
                error = e
            }
        }
        
        if let error = error { throw error }
    }
    
    open override func persist(_ entities: [Model]) throws {
        var error: Error?
        
        let context = self.context
        
        context.performAndWait { [weak self] in
            guard let `self` = self else { return }
            
            entities.forEach{ entity in
                if self.isEntryExist(entity.entityId, inContext: context) {
                    let existingEntries = self.fetchEntries(entity.entityId, inContext: context)
                    existingEntries.forEach {
                        self.translator.fill($0, fromEntity: entity, in: context)
                    }
                } else if let entry = NSEntityDescription.insertNewObject(
                    forEntityName: self.translator.entryClassName,
                    into: context) as? CDModel {
                    
                    self.translator.fill(entry, fromEntity: entity, in: context)
                }
            }
            
            do {
                try context.save()
            } catch let e {
                error = e
                context.rollback()
            }
        }
        
        if let error = error { throw error }
    }
    
    open override func read(_ entityId: String) -> Model? {
        guard let entries = try? context.fetch(request(entityId)),
            !entries.isEmpty,
            let entry = entries.first
            else {
                return nil
        }
        
        let entity = Model()
        translator.fill(entity, fromEntry: entry)
        
        return entity
    }
    
    open override func read() -> [Model] {
        return fetchEntries(nil, sortDescriptors: [], inContext: context)
            .flatMap {
                let entity = Model()
                
                self.translator.fill(entity, fromEntry: $0)
                
                return entity
        }
    }
    
    override open func erase() throws {
        var error: Error?
        
        let context = self.context
        
        context.performAndWait { [weak self] in
            guard let `self` = self else { return }
            
            self.fetchEntries(inContext: context)
                .forEach {
                    context.delete($0)
            }
            
            do {
                try context.save()
            } catch let e {
                error = e
                context.rollback()
            }
        }
        
        if let error = error { throw error }
    }
    
    //MARK: - Transactions
    
    private func create(_ entity: Model, inContext context: NSManagedObjectContext) throws {
        guard let entry = NSEntityDescription.insertNewObject(
            forEntityName: translator.entryClassName,
            into: context) as? CDModel
            else {
                return
        }
        
        translator.fill(entry, fromEntity: entity, in: context)
        
        try context.save()
    }
    
    private func update(_ entity: Model, inContext context: NSManagedObjectContext) throws {
        let existingEntries = fetchEntries(entity.entityId, inContext: context)
        
        existingEntries.forEach {
            translator.fill($0, fromEntity: entity, in: context)
        }
        
        try context.save()
    }
    
    private func isEntryExist(_ entryId: String, inContext context: NSManagedObjectContext) -> Bool {
        let existingEntries = fetchEntries(entryId, inContext: context)
        return existingEntries.count > 0
    }
    
    //MARK: - Private
    
    private func request(_ entryId: String) -> NSFetchRequest<CDModel> {
        let request = NSFetchRequest<CDModel>(entityName: translator.entryClassName)
        request.predicate = NSPredicate(format: "entryId == %@", argumentArray: [entryId])
        
        return request
    }
    
    private func fetchEntries(_ entryId: String, inContext context: NSManagedObjectContext) -> [CDModel] {
        if let entries = try? context.fetch(request(entryId)) {
            return entries
        } else {
            return [CDModel]()
        }
    }
    
    private func fetchEntries(_ predicate: NSPredicate? = nil, sortDescriptors: [NSSortDescriptor] = [], inContext context: NSManagedObjectContext) -> [CDModel] {
        if let entries = try? context.fetch(request(predicate, sortDescriptors: sortDescriptors)) {
            return entries
        } else {
            return [CDModel]()
        }
    }
    
    private func request(_ predicate: NSPredicate?,
                         sortDescriptors: [NSSortDescriptor]) -> NSFetchRequest<CDModel> {
        let request = NSFetchRequest<CDModel>(entityName: translator.entryClassName)
        request.predicate = predicate
        request.sortDescriptors = sortDescriptors
        
        return request
    }
    
}
