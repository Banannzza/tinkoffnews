//
//  API.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 19/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

enum API {
    case news
    case newsSection(Int, Int)
    case newsContent(String)
    
    var stringValue: String {
        get {
            switch self {
            case .news:
                return "https://api.tinkoff.ru/v1/news"
            case .newsSection(let first, let last):
                return "https://api.tinkoff.ru/v1/news?first=\(first)&last=\(last)"
            case .newsContent(let id):
                return "https://api.tinkoff.ru/v1/news_content?id=\(id)"
            }
        }
    }
}
