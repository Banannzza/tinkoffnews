//
//  Response.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 19/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

enum Response<ObjectType> {
    case fail(Error?)
    case success(ObjectType)
}
