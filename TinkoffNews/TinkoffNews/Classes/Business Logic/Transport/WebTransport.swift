//
//  WebTransport.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 19/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation


final class WebTransport {
    
    //MARK: - Property
    
    private let defaultSession: URLSession
    private var dataTask: URLSessionTask?
    
    init(configuration: URLSessionConfiguration) {
        defaultSession = URLSession(configuration: configuration)
    }
    
    //MARK: - Methods
    
    public func makeRequest(forURL: String, completionHandler: @escaping (Response<Data>) -> Void) {
        dataTask?.cancel()
        if let url = URL(string: forURL) {
            dataTask = defaultSession.dataTask(with: url) { data, response, error in
                defer { self.dataTask = nil }
                
                if let error = error {
                    completionHandler(.fail(error))
                } else if let data = data,
                    let response = response as? HTTPURLResponse,
                    response.statusCode == 200 {
                    completionHandler(.success(data))
                }
            }
            dataTask?.resume()
        }
    }
}
