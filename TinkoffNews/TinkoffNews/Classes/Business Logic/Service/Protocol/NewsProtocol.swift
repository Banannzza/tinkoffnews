//
//  NewsProtocol.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 19/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

protocol NewsProtocol {
    
    /// Получение новостей
    ///
    /// - Parameter completionHandler: ответ от сервера, который может содержать новости
    func newsList(completionHandler: @escaping (Response<[NewsModel]>) -> Void)
    
    /// Получение новостей
    ///
    /// - Parameter first: Начало ограничения количества сущностей
    /// - Parameter last: Конец ограничения количества сущностей
    /// - Parameter completionHandler: ответ от сервера, который может содержать новости
    func newsList(first: Int, last: Int, completionHandler: @escaping (Response<[NewsModel]>) -> Void)
    
    /// Получение сохраненых новостоей
    func savedNewsList() -> [NewsModel]
    
    /// Получение подробной информации
    /// - Parameter newsId: id выбранной новости
    /// - Parameter completionHandler: ответ от сервера, который может содержать полную информацию по выбранной новости
    func newsContent(newsId: String, completionHandler: @escaping (Response<String>) -> Void)
    
    func updateNewsTapCounter(newsId: String) -> NewsModel
    
}
