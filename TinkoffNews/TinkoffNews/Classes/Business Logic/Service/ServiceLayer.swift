//
//  ServiceLayer.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 19/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation
import CoreData

final class ServiceLayer {
    
    static let shared = ServiceLayer()
    
    let newsService: NewsProtocol
    
    private init() {
        do {
            let dao = try CoreDataDAO<NewsModelEntity, NewsModel>(containerName: "TinkoffNews", translator: NewsModelCoreDataTranslator<NewsModelEntity>())
            newsService = NewsService(dao: dao)
        } catch {
            fatalError(error.localizedDescription)
        }
    }
}
