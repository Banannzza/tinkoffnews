//
//  NewsService.swift
//  TinkoffNews
//
//  Created by Алексей Остапенко on 19/03/2018.
//  Copyright © 2018 Алексей Остапенко. All rights reserved.
//

import Foundation

class NewsService: NewsProtocol {
    
    //MARK: - Property
    
    private let transport: WebTransport
    private let dao: DAO<NewsModel>
    
    init(dao: DAO<NewsModel>) {
        transport = WebTransport(configuration: .default)
        self.dao = dao
//        try! dao.erase()
    }
    
    //MARK: - Private
    
    private func saveNews(_ list: [NewsModel]) {
        DispatchQueue.main.async {
            let localNews = Set<NewsModel>(self.dao.read().map({ $0 }))
            let newNews = Set<NewsModel>(list.map({ $0 }))
            let newsForAppend = newNews.subtracting(localNews)
            do {
                try self.dao.persist(Array<NewsModel>(newsForAppend))
            } catch {
                fatalError(error.localizedDescription)
            }
        }
    }
    
    private func newsListRequest(forURL: String, completionHandler: @escaping (Response<[NewsModel]>) -> Void) {
        transport.makeRequest(forURL: forURL) { (response) in
            switch response {
            case .fail(let error):
                completionHandler(.fail(error))
            case .success(let data):
                do {
                    if let responseDictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                        let newsList: [NewsModel] = ObjectParser.parseObject(fromJson: responseDictionary)
                        self.saveNews(newsList)
                        completionHandler(.success(newsList))
                        
                    }
                } catch {
                    completionHandler(.fail(error))
                }
            }
        }
    }
    
    //MARK: - Public
    
    func newsList(completionHandler: @escaping (Response<[NewsModel]>) -> Void) {
        self.newsListRequest(forURL: API.news.stringValue, completionHandler: completionHandler)
    }
    
    func newsList(first: Int, last: Int, completionHandler: @escaping (Response<[NewsModel]>) -> Void) {
        self.newsListRequest(forURL: API.newsSection(first, last).stringValue, completionHandler: completionHandler)
    }
    
    func updateNewsTapCounter(newsId: String) -> NewsModel {
        let model = dao.read(newsId)!
        model.tapCount += 1
        try! dao.persist(model)
        return model
    }
    
    func newsContent(newsId: String, completionHandler: @escaping (Response<String>) -> Void) {
        transport.makeRequest(forURL: API.newsContent(newsId).stringValue) { (response) in
            switch response {
            case .fail(let error):
                completionHandler(.fail(error))
            case .success(let data):
                do {
                    if let responseDictionary = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any],
                    let patload = responseDictionary["payload"] as? [String: Any] {
                        if let newsContent = patload["content"] as? String {
                            completionHandler(.success(newsContent))
                        } else {
                            completionHandler(.fail(nil))
                        }
                    }
                } catch {
                    completionHandler(.fail(error))
                }
            }
        }
    }
    
    func savedNewsList() -> [NewsModel] {
        return self.dao.read()
    }
    
}
